<?php require_once 'inc/config.php' ;?>
<!DOCTYPE html>
  <html>
  <head>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.css"/>
  </head>
  <body>
    <div class="container">
      <?php require_once 'Template/header.php' ;?>
      <div class="contenu">
        <h1>Ajouter une tâche</h1>
      <form method="post" action="insert.php">
      <p>Description de la tâche</p>
      <TEXTAREA name="description" rows=4 cols=40></TEXTAREA>
      <label for="date">Date</label>
      <input name="due_at"type="text"/></p>
      <label for="deadline">Deadline</label>
      <input name="deadline"type="text"/></p>
      <label for="priority">Priority</label>
      <select name="priority">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
      </select>
      <div class="bouton">
      <input type="submit" name="sauver" value="Sauver">
      </form>
    </div>
      </main>
     </div>
        <?php require_once 'Template/footer.php' ;?>
    </div>
  </body>
  </html>
