<?php require_once 'inc/config.php' ;?>
<!DOCTYPE html>
  <html>
  <head>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.css"/>
  </head>
  <body>
    <div class="container">
            <?php require_once 'Template/header.php' ;?>
        <div class="tache">
        <ul class="tasklist-titre">
            <li class="tasklist-item-titre">
            <span class="tasklist-item-id">#</span>
            <span class="tasklist-item-description">Description</span>
            <span class="tasklist-item-creation">Date de cration</span>
            <span class="tasklist-item-due">Deadline</span>
            <span class="tasklist-item-priority">Priorité</span>
        </li>
      </ul>
            <?php foreach ($data as $row):?>
          <ul class="tasklist">
            <li class="<?php if($row['status'] =='ok'):?>is-done<?php endif;?>">
              <span class="tasklist-item-id"><?php echo $row['id']?></span>
                <span class="tasklist-item-description"><?php echo $row['description']?></span>
                <span class="tasklist-item-creation"><?php echo $row['created_date']?></span>
                <span class="tasklist-item-due"><?php echo $row['due_at']?></span>
                <span class="tasklist-item-priority"><?php echo $row['priority']?></span>
                <a href="update.php?task=<?php echo $row['id'];?>" class="fa fa-check" aria-hidden="true"></i></a>
                <a href="delete.php?task=<?php echo $row['id'];?>" class="fa fa-window-close"></a>
          </li>
          </ul>
           <?php endforeach?>
           <a href="edit.php"><i class="fa fa-plus-square" aria-hidden="false"></a></i>
         </div>
           <?php require_once 'Template/footer.php' ;?>

  </body>
  </html>
